# OpenML dataset: datatrieve

https://www.openml.org/d/1075

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
This is a PROMISE Software Engineering Repository data set made publicly
available in order to encourage repeatable, verifiable, refutable, and/or
improvable predictive models of software engineering.

If you publish material based on PROMISE data sets then, please
follow the acknowledgment guidelines posted on the PROMISE repository
web page http://promise.site.uottawa.ca/SERepository .
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
1. Title/Topic: The transition of the DATATRIEVE product from version 6.0 to
version 6.1

2. Sources:
-- Creators: DATATRIEVETM project carried out at Digital Engineering Italy
-- Donor: Guenther Ruhe
-- Date:  January 15, 2005
3. Past usage:

A hybrid approach to analyze empirical software engineering data
and its application to predict module fault-proneness in maintenance
Source 	Journal of Systems and Software archive
Volume 53 ,  Issue 3  (September 2000) table of contents
Pages: 225 - 237
Year of Publication: 2000
ISSN:0164-1212
Authors
Sandro Morasca
Gunther Ruhe
4. Relevant information:

The DATATRIEVE product was undergoing both adaptive (DATATRIEVE was being transferred
from platform OpenVMS/VAX to platform OpenVMS/Alpha) and corrective maintenance
(failures reported from customers were being fixed) at the Gallarate (Italy)
site of Digital Engineering.

The DATATRIEVE product was originally developed in the BLISS language. BLISS is an
expression language. It is block-structured, with exception handling facilities, coroutines,
and a macro system. It was one of the first non-assembly languages for operating system
implementation.. Some parts were later added or rewritten in the C language. Therefore, the
overall structure of DATATRIEVE is composed of C functions and BLISS subroutines.

The empirical study of this data set reports only the BLISS part, by far the bigger one.
In what follows, we will use the term "module" to refer to a BLISS module, i.e., a set of
declarations and subroutines usually belonging to one file. More than 100 BLISS modules
have been studied. It was important to the DATATRIEVE team to better understand how the
characteristics of the modules and transition process were correlated with the code quality.

The objective of the data analysis was to study whether it was possible to classify modules as
non-faulty or faulty, based on a set of measures collected on the project.

5. Number of records: 130
6. Number of attributes: 9
8 condition attributes
1 decision attribute
7. Attribute Information:

1. LOC6_0: number of lines of code of module m in version 6.0.
2. LOC6_1: number of lines of code of module m in version 6.1.
3. AddedLOC: number of lines of code that were added to module m in version 6.1, i.e., they
were not present in module m in version 6.0.
4. DeletedLOC: number of lines of code that were deleted from module m in version 6.0, i.e.,
they were no longer present in module m in version 6.1.
5. DifferentBlocks: number of different blocks module m in between versions 6.0 and 6.1.
6. ModificationRate: rate of modification of module m, i.e.,
(AddedLOC + DeletedLOC) / (LOC6.0 + AddedLOC).
7. ModuleKnowledge: subjective variable that expresses the project team's knowledge on
module m (low or high)
8. ReusedLOC: number of lines of code of module m in version 6.0 reused in module m in
version 6.1.
9. Faulty6_1: its value is 0 for all those modules in which no faults were found;
its value is 1 for all other modules.

8. Missing attributes: none

9. Class Distribution:
0:   119 = 91.54%
1:   11  =  8.46%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1075) of an [OpenML dataset](https://www.openml.org/d/1075). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1075/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1075/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1075/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

